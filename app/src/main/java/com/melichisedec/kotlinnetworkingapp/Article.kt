package com.melichisedec.kotlinnetworkingapp

/**
 * Created by mwine on 10/14/2017.
 */
data class Article ( val mTitle : String, val mUrl : String )